// ----------- IMPORTS --------------------//
import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'
import 'firebase/database'
// --------------------------------------//

// Iniciar o Firebase
  var config = {
    apiKey: "AIzaSyAIuXQDDRQBvYbkNehpyYcnOB2O0WVTmp0",
    authDomain: "wish-it-lab5.firebaseapp.com",
    databaseURL: "https://wish-it-lab5.firebaseio.com",
    projectId: "wish-it-lab5",
    storageBucket: "",
    messagingSenderId: "639576736690"
  };

  firebase.initializeApp(config);
  firebase.firestore().settings({ timestampsInSnapshots: true});
  firebase.database();

  export default firebase;
