// ----------- IMPORTS --------------------//
import React, { Component } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Navbar from './components/layout/Navbar';
import User from './components/auth/User_page';
import PaginaInicial from './components/dashboard/PaginaInicial';
import Search from './components/dashboard/Search';
import Registo from './components/auth/Registo';
import Login from './components/auth/Login';
import EditarDados from './components/auth/EditarDados';
import Dashboard from './components/dashboard/Dashboard';
import WishList from './components/dashboard/WishList';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
// --------------------------------------//

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="App">
          <Switch>
          <Route exact path='/' component={ Login } />
          <Route path='/login' component={ Login } />
          <Route path='/user_page' component= { User } />
          <Route path='/paginainicial' component= { PaginaInicial } />
          <Route path='/search' component= { Search } />
          <Route path='/wishlist' component= { WishList } />
          <Route path='/registo' component={Registo} />
          <Route path='/editardados' component={EditarDados} />
          </Switch>
          </div>
          </BrowserRouter>
    );
  }
}

export default App;
