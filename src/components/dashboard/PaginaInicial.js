// ----------- IMPORTS --------------------//
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { firestoreConnect } from 'react-redux-firebase';
import { Redirect } from 'react-router-dom';
import  ViewList  from '../views/ViewList';
import Navbar from '../../components/layout/Navbar';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeart } from "@fortawesome/free-regular-svg-icons";
import  tlmv from '../../img/tlmv.png';
// --------------------------------------//


class PaginaInicial extends Component {
  render() {

    const { views, auth, profile } = this.props;

    if (!auth.uid) {
      return <Redirect to='/login'/>;
    }

    return (
      <div>
      <Navbar />
      <header className=" text-center text-white d-flex" style={{backgroundImage: "url('../img/bg.jpg')"}}>
        <div className="container">
          <div className="row">
            <div className="col-lg-8 mx-auto">
              <p />
                <label style={{fontFamily: "Arial, bold", fontSize: "30px", color: "rgb(35, 54, 86)"}} className="mt-5">Bem-Vindo(a), {profile.nome}!</label>
              <br />
              <hr />
              <small style={{fontFamily: "Arial", fontSize: "15px", color: "rgb(35, 54, 86)"}}>  Destacamos alguns produtos que visitou recentemente. <br /> Talvez esteja interessado(a). </small>
              <hr />
            </div>
          </div>
        </div>
      </header>

      <ViewList views={views}/>
    </div>
    )
  }
};

const mapStateToProps = (state) => {
  return {
       views: state.firestore.ordered.views,
       auth: state.firebase.auth,
       profile: state.firebase.profile
  }
}

export default compose(
  connect(mapStateToProps),
  firestoreConnect(props => [
    { collection: 'views',
      where: [['userId', '==', props.auth.uid]],
      limit: 6,
      orderBy: ['createdAt', 'desc']
    }
  ])
)(PaginaInicial);
