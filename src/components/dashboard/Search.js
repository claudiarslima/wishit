// ----------- IMPORTS --------------------//
import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import Navbar from '../../components/layout/Navbar';
import Background from "../dashboard/Background";
// --------------------------------------//

class Search extends Component {

  state = {
       input: ''
   };

  render() {

const { auth } = this.props;

    if (!auth.uid) {
      return <Redirect to='/login'/>;
    }


    return (

      <div className="Search">

      <div className="container">
          <div className="row">
              <div className="col-lg-8 mx-auto">
                  <p />
                  <label style={{fontFamily: "Arial", fontSize: "20px", color: "rgb(35, 54, 86)", marginTop: "110px"}}>Procura algum produto em especial?</label>
                  <p />
                  <div className="App">
                <input onChange={(event) => this.onChange(event)} type="text" placeholder="Procure um Produto"  style={{backgroundColor: "white", padding:"15px", borderRadius: "15px", width: "300px", height:"40px", border:"none", fontFamily: "Arial", fontSize: "13px"}} />

                </div>
                  <small style={{fontFamily: "Arial", fontSize: "10px", color: "rgb(35, 54, 86)"}}>Exemplo: iphone.</small>
                  <br />
                  <br />
                  <label style={{fontFamily: "Arial", fontSize: "10px", color: "white", opacity: "41%", backgroundColor: "#7B9BA6",  borderRadius: "5px", padding: "30px"}} className="mt-auto mb-4">Não encontrou o que procurava? <br /> Fale connosco! <br /> Envie um email para: wishitapp@wish.com</label>
              </div>
          </div>

      </div>
      <Background pagina = '1' text={this.state.input} />
      <Navbar />
      </div>
    );
  }
  onChange(event) {
     this.setState({input:event.target.value});
 }
};

const mapStateToProps = (state) => {
  return {
       auth: state.firebase.auth
  }
}

export default connect(mapStateToProps)(Search);
