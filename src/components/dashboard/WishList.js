// ----------- IMPORTS --------------------//
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Delete } from '../../store/actions/likeActions';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { firestoreConnect } from 'react-redux-firebase';
import { Redirect } from 'react-router-dom';
import Navbar from '../../components/layout/Navbar';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
 import { Button } from 'reactstrap';
 import { faTimes } from "@fortawesome/free-solid-svg-icons";
 import { faHeart } from '@fortawesome/free-solid-svg-icons';
 // --------------------------------------//

class WishList extends Component {

    state = {
      delete: false,
      idprod: ' ',
      userId: ' ',
      idlog:''
    }

  delete(id, userid){

    this.setState({delete: true, userId: {userid}})

    this.props.Delete({delete: true, idprod: id, userId: userid})
}

  render() {

    const { likes, auth } = this.props;
    // this.setState({idlog: this.props.auth.uid})
    if (!auth.uid) {
      return <Redirect to='/login'/>;
    }



  return (
    <div>
    <Navbar />
    <header className=" text-center text-white d-flex" style={{backgroundImage: "url('img/bg.jpg')", opacity:"0.8"}}>
      <div className="container">
        <div className="row">
          <div className="col-lg-8 mx-auto">
            <p />
            <label style={{fontFamily: "Arial", fontSize: "30px", color: "rgb(35, 54, 86)"}} className="mt-5">Lista de Desejos</label>
            <br />
            <hr />
            <small style={{fontFamily: "Arial", fontSize: "15px", color: "rgb(35, 54, 86)"}}> Aqui pode encontrar os produtos que guardou na sua lista: </small>
            <p />
          </div>
        </div>
      </div>
    </header>
    <section className="contact-section bg-black mt-4" style={{marginBottom:"100px"}}>
      <div className="container">
      <div className="row">
          { likes && likes.map(like => {

            return(
            <div key={like} className="col-md-4">

          <div className="card mb-3" style={{backgroundColor:"rgb(249,247,247)", border: "1px solid rgba(0,0,0,.125)", boxShadow: "2px 2px 2px #ffc", WebkitBoxShadow: "4px 4px 4px #ccc", MozBoxShadow: "4px 4px 4px #ccc"}}>

          <div className="card-header" style={{backgroundColor: "rgb(35, 54, 86)", color: "white", fontFamily: "Arial", fontWeight: "bold", fontSize: "20px"}}>
          Produto
          <Button onClick={()=> this.delete(like.idprod, like.userId)} style={{float:"right", backgroundColor:"transparent", border:"none", padding:"0"}}>
          <FontAwesomeIcon value={like.idprod} icon={faTimes} size="1x" color="white" className="mt-1" style={{float:"right"}}/>
          </Button>

          </div>

            <div className="card-body" value={like.userId} style={{height:"297px"}}>

            <img src={like.imgprod} alt="Telemovel" className="img-fluid m-0" style={{height: "100px", width: "100px"}} />
            <h4 className=" m-0 mt-4">{like.nomeprod.length <20 ? `${like.nomeprod}` : `${like.nomeprod.substring(0,25)}...`}</h4>
            <h4 className="text-uppercase m-0 mt-4" style={{fontSize: "13px" }}>price: {like.priceprod + '€'} </h4>


            </div>
            <div className="card-footer text-muted" style={{backgroundColor: "rgb(229, 227,227)"}}>

              <FontAwesomeIcon icon="heart" color="rgb(35, 54, 86)" style={{float:"right"}}/>

           </div>
          </div>
          </div>
          )})}
      </div>
      </div>

    </section>
  </div>
  );

};
}


const mapStateToProps = (state) => {
  return {
       likes: state.firestore.ordered.likes,
       auth: state.firebase.auth,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  Delete: (like) => dispatch(Delete(like))
 }
}

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  firestoreConnect(props =>[
    { collection: 'likes',
    where: [['userId', '==', props.auth.uid]],
    orderBy: ['createdAt', 'desc']
   }
  ])
)(WishList);
