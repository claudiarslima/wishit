// ----------- IMPORTS --------------------//
import React, {Component} from 'react';
import { connect } from 'react-redux';
import { Button, Pagination, PaginationItem, PaginationLink } from 'reactstrap';
import { Redirect } from 'react-router-dom';
import { Glyphicon } from 'react-bootstrap';
import { Likes } from '../../store/actions/likeActions';
import { Views } from '../../store/actions/viewActions';
import LikeButton from '../buttons/LikeButton';
import IconButton from '@material-ui/core/IconButton';
import  FavoriteBorder  from '@material-ui/icons/FavoriteBorder';
import  Favorite  from '@material-ui/icons/Favorite';
import  AddCircle from '@material-ui/icons/AddCircle';
import  RemoveCircle from '@material-ui/icons/RemoveCircle';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeart, faStar } from '@fortawesome/free-solid-svg-icons';
import { faHeart as faHeartRegular } from '@fortawesome/free-regular-svg-icons'
import { library } from '@fortawesome/fontawesome-svg-core';

// --------------------------------------//
library.add(faHeart, faStar, faHeartRegular);

class Background extends Component {
    constructor(props) {
        super(props)
        let pagina = 1, anterior, proxima, inicial, ultima
        this.state = {
          posts: [],
          name_state: '',
          idprod: ' ',
          imgprod: ' ',
          nomeprod: ' ',
          priceprod: ' ',
          idview: ' ',
          imgview: ' ',
          nomeview: ' ',
          pagina: '1',
          lastpage: '',
          display: 'none'
        };

    }



    handleClick = (e, id, img, nome, price) => {
			e.preventDefault();

        this.props.Likes({idprod: id[0], imgprod: img[0], nomeprod: nome[0], priceprod: price});

    }

    moreInfo = (id, img, nome) => {
        this.props.Views({idview: id[0], imgview: img[0], nomeview: nome[0]});
    }

    fetchFirst(url, page) {
        var that = this;
        if (url) {

            var proxyUrl = 'https://cors-anywhere.herokuapp.com/',
                targetUrl = 'http://svcs.ebay.com/services/search/FindingService/v1?OPERATION-NAME=findItemsByKeywords&SERVICE-VERSION=1.0.0&SECURITY-APPNAME=InsMende-whisit-PRD-660c321e5-141698e5&GLOBAL-ID=EBAY-US&RESPONSE-DATA-FORMAT=JSON&keywords=' + url + '&paginationInput.entriesPerPage=21&paginationInput.pageNumber='+page;

            fetch(proxyUrl + targetUrl).then(function (response) {
                return response.json();
            }).then(function (result) {
                that.setState({posts: result.findItemsByKeywordsResponse[0].searchResult[0].item, display: "block", lastpage: result.findItemsByKeywordsResponse[0].paginationOutput[0].totalPages[0]});

                }
            )
        }
    }

    // componentWillMount(search, pagi) {
    //         this.fetchFirst(search, pagi);
    //
    // }


getinfo = (s) => {

  if(s !== this.search && s.length > 2){
    this.search = s
  this.fetchFirst(s, this.state.pagina)
}
}


pagination = (pagina, anterior, proxima, inicial, ultima ) => {
  if(ultima > 100) {
    ultima = 100
  }

inicial = 1

// if(this.lastpage > 1) {
//   pagina = parseInt(pagina)
//     anterior = pagina-1
//     proxima = pagina + 1
//     }

    if(pagina == 1) {
      pagina = parseInt(pagina)
      anterior = pagina
      proxima = pagina + 1
    }
    if (pagina > 1 && pagina < ultima) {
      pagina = parseInt(pagina)
      anterior = pagina - 1
      proxima = pagina + 1
    }
    if (pagina == ultima){
      pagina = parseInt(pagina)
      anterior = pagina - 1
      proxima = pagina
    }


  return (
    <div className="m-auto" style={{display: this.state.display}}>
    <Pagination style={{marginTop:"40px", color:"grey"}}>
        <PaginationLink onClick={() => {
          this.setState({pagina: inicial})
          this.fetchFirst(this.props.text, inicial)}} style={{color:"grey"}}> « </PaginationLink>

          <PaginationLink onClick={() => {
            this.setState({pagina: anterior})
            this.setState({posts: []})
            this.fetchFirst(this.props.text, anterior)}} style={{color:"grey"}}>back</PaginationLink>


            <PaginationLink style={{color:"grey"}}>{this.state.pagina}</PaginationLink>

            <PaginationLink onClick={() => {
              this.setState({pagina: proxima})
              this.setState({posts: []})
              this.fetchFirst(this.props.text, proxima)}} style={{color:"grey"}}>next</PaginationLink>

          <PaginationLink onClick={() => {
            this.setState({pagina: ultima})
            this.fetchFirst(this.props.text, ultima)}} style={{color:"grey"}}>»</PaginationLink>
</Pagination>
      </div>
  )
}



    render() {
     return (
         <div className="">
         <section className="contact-section bg-black mt-4" style={{marginBottom:"100px"}}>
         <div className="container">
         <div className="row">
             {this.state.posts === undefined ?
               <div className="col-lg-12 mx-auto" style={{height:"100px",  border: "1px solid rgba(0,0,0,.125)", padding:"35px", backgroundColor:"lightgray"}}>Produto não encontrado!</div>
               :
               this.state.posts.map((data, key) => {

                return(
                  <div className="col-md-4" key={`item_${key}`}>
                    <div className="card mb-3" style={{backgroundColor:"rgb(249,247,247)", border: "1px solid rgba(0,0,0,.125)", boxShadow: "2px 2px 2px #ffc", webkitBoxShadow: "4px 4px 4px #ccc", mozBoxShadow: "4px 4px 4px #ccc"}}>

                    <LikeButton handleClick={this.handleClick} data={data} moreInfo={this.moreInfo} />
                    </div>
                   </div>
                )
              }
             )}
             {this.pagination(this.state.pagina, this.anterior, this.proxima, this.inicial, this.state.lastpage)}

             </div>
             </div>
             </section>
         </div>
     );
 }

 componentDidUpdate(prevProps){
   if(prevProps.text !== this.props.text){

     this.getinfo(this.props.text)
   }
 }
}

   const mapDispatchToProps = dispatch =>
   {
     return {
     Likes: (like) => dispatch(Likes(like)),
     Views: (view) => dispatch(Views(view))
   }
   }

export default connect(null, mapDispatchToProps)(Background);
