import React, { Component } from 'react';
import Login from '../auth/Login.js'
import { firestoreConnect } from 'react-redux-firebase'
import 'bootstrap/dist/css/bootstrap.min.css';

class Dashboard extends Component {
  render() {
    return(
      <div className="masthead text-center text-white d-flex">
        <div class="container my-auto" >
        <div className="row">
          <div className="col-lg-8 mx-auto">
          <Login />
          </div>
        </div>
      </div>
      </div>
    )
  }
}

export default Dashboard;
