// ----------- IMPORTS --------------------//
import React from 'react';
import { NavLink } from 'react-router-dom';
import { faHome } from "@fortawesome/free-solid-svg-icons";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { faHeart } from "@fortawesome/free-solid-svg-icons";
import { faUser } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// --------------------------------------//

const Navbar = () => {
  return(
    <nav className="navbar nav-wrapper fixed-bottom navbar-expand" style= {{backgroundColor: "#233656"}}>
  <div className="container">
      <ul className="navbar-nav mx-auto">
          <li className="nav-item">
              <NavLink className="nav-link js-scroll-trigger" to="/PaginaInicial"><FontAwesomeIcon icon={faHome} style={{color: "white"}}/> </NavLink>
          </li>
          <li className="nav-item">
              <NavLink className="nav-link js-scroll-trigger" to="/Search"><FontAwesomeIcon icon={faSearch} style={{color: "white"}}/> </NavLink>
          </li>
          <li className="nav-item">
              <NavLink className="nav-link js-scroll-trigger" to="/wishlist"><FontAwesomeIcon icon={faHeart} style={{color: "white"}}/> </NavLink>
          </li>
          <li className="nav-item">
              <NavLink className="nav-link js-scroll-trigger" to="/user_page"><FontAwesomeIcon icon={faUser} style={{color: "white"}}/> </NavLink>
          </li>
      </ul>
  </div>
</nav>
  )
}

export default Navbar;
