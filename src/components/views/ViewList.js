// ----------- IMPORTS --------------------//
import React from 'react';
import ViewSummary from './ViewSummary.js'
// --------------------------------------//

const ViewList = ({views}) => {
  return(
    <section className="contact-section bg-black mt-4" style={{marginBottom:"100px"}}>
      <div className="container">
      <div className="row">
          { views && views.map(view => {
            
            return(
            <ViewSummary view={view} key={view.id} />
          )})}
      </div>
      </div>

    </section>
  )
}

export default ViewList
