// ----------- IMPORTS --------------------//
import React from 'react'
import { connect } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from "@fortawesome/free-solid-svg-icons";
import moment from 'moment';
// --------------------------------------//

const ViewSummary = ({view}) => {

  return(

    <div className="col-md-4 mb-5 ">
    <div className="card h-100 mb-5" style={{backgroundColor:"rgb(249,247,247)", border: "1px solid rgba(0,0,0,.125)", boxShadow: "2px 2px 2px #ffc", webkitBoxShadow: "4px 4px 4px #ccc", mozBoxShadow: "4px 4px 4px #ccc"}}>
    <div className="card-header" style={{backgroundColor: "rgb(35, 54, 86)", color: "white", fontFamily: "Arial", fontWeight: "bold", fontSize: "20px"}}>Produto</div>
      <div className="card-body">
      <img src={view.imgview} alt="Telemovel" className="img-fluid m-0 mt-4 rounded" style={{height: "120px", width: "120px"}} />
      <h4 className="m-0 mt-4">{view.nomeview.length <20 ? `${view.nomeview}` : `${view.nomeview.substring(0,25)}...`}</h4>
      <h6 className="m-0 mt-4">{moment(view.createdAt.toDate().toString()).calendar()}</h6>
      <div className="small text-black-50 text-right">
      </div>

      </div>
    </div>
    </div>
  )
}


export default connect(null,null)(ViewSummary);
