// ----------- IMPORTS --------------------//
import React, {Component} from 'react';
import { Button } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { getFirebase } from 'react-redux-firebase';
import { getFirestore } from 'redux-firestore';
import { faHeart } from '@fortawesome/free-solid-svg-icons';
import { library } from '@fortawesome/fontawesome-svg-core';
import  AddCircle from '@material-ui/icons/AddCircle';
import  RemoveCircle from '@material-ui/icons/RemoveCircle';
import IconButton from '@material-ui/core/IconButton';
import { faHeart as faHeartRegular } from '@fortawesome/free-regular-svg-icons'
// --------------------------------------//

library.add(faHeart, faHeartRegular);

class LikeButton extends Component {
  constructor(props){
    super(props)

    this.state = {
      like: false,
      display1: "block",
      display2: "none"
    }
  }

  lessInfo() {
    if(this.state.display1 === "block" || this.state.display2 === "none"){
      this.setState({display2:"block",display1:"none"});
    }else{
      this.setState({display2:"none", display1:"block"});
    }
  }

  componentDidUpdate(prevProps){

    if(prevProps.data !== this.props.data){
      const firebase = getFirebase();
      const firestore = getFirestore();

      this.setState({like: false}, ()=>{
         firestore.collection('likes').where('idprod', '==', this.props.data.itemId[0].toString()).get().then((querySnapshot) => {
              querySnapshot.forEach((doc) => {
                this.setState({like: true})
              })
            }).catch((error)=> {
              console.log("error");
            });
      })

    }
  }


  componentDidMount(){
    const firebase = getFirebase();
   const firestore = getFirestore();


   firestore.collection('likes').where('idprod', '==', this.props.data.itemId[0].toString()).get().then((querySnapshot) => {
              querySnapshot.forEach((doc) => {
                this.setState({like: true})
              })
            }).catch((error)=> {
              console.log("error");
            });;
  }

  render() {
    let {handleClick, data, moreInfo} = this.props;

  return(
    <div className="">
     <div className="card-header" style={{backgroundColor: "rgb(35, 54, 86)", color: "white", fontFamily: "Arial", fontWeight: "bold", fontSize: "20px"}}>Produto</div>

     <div className="card-body" style={{ display: this.state.display1, height:"297px"}}>
         <img src={data.galleryURL} alt="Telemovel" className="img-fluid rounded"
              style={{height: "110px", width: "110px"}}/>
         <h4 className=" m-0 mt-5 mb-3 font-weight-bold" style={{maxWidth:"10"}} value={data.title}
             style={{fontSize: "10px"}}>{data.title}</h4>
         <h4 className="text-uppercase m-0 mt-4" style={{fontSize: "10px"}}>
             Price: {data.sellingStatus[0].currentPrice[0].__value__ + "€"}</h4>
         <h4 className="text-uppercase m-0 mt-4" style={{fontSize: "10px"}}>
             Shipping Cost: {data.shippingInfo[0].shippingType}</h4>
     </div>

     <div className="card-footer text-muted" style={{textAlign:"right", display: this.state.display1, backgroundColor: "rgb(229, 227,227)"}} >
     <IconButton className="mr-2 "style={{ color:"rgb(35, 54, 86)", padding:"0"}}>
         <AddCircle onClick={() => {
           if(this.state.display1=="block" || this.state.display2=="none"){
             this.setState({display2:"block",display1:"none"});
           }else{
             this.setState({display2:"none", display1:"block"});
           }
           moreInfo(data.itemId, data.galleryURL, data.title);
         }}/>
     </IconButton>
        <Button disabled={this.state.like} id={data.itemId} className="mr-2" style={{backgroundColor:"transparent", border:"none", color:"rgb(35, 54, 86)", padding:"0"}}
            onClick={e => {
              this.setState({like:true});
              handleClick(e, data.itemId, data.galleryURL, data.title, data.sellingStatus[0].currentPrice[0].__value__);
            }
          }>
            { this.state.like ? <FontAwesomeIcon icon={faHeart}/>
                : <FontAwesomeIcon icon={faHeartRegular}/> }
        </Button>
    </div>

    <div className="card-body" style={{ display: this.state.display2, height:"297px" }}>
    <img src={data.galleryURL} alt="Telemovel" className="img-fluid rounded" style={{height: "110px", width: "110px"}} />
      <h4 className="text-uppercase m-0 mt-5" value={data.subtitle}
      style={{fontSize: "10px"}}>Subtitle: {data.subtitle}</h4>
      <h4 className="text-uppercase m-0 mt-4"
      style={{fontSize: "10px" }}>Shipping Location: {data.shippingInfo[0].shipToLocations}</h4>
      <h4 className="text-uppercase m-0 mt-4"
      style={{fontSize: "10px" }}>Category: {data.primaryCategory[0].categoryName}</h4>
      <h4 className="text-uppercase m-0 mt-3"
      style={{fontSize: "10px" }}><a target="_blank" href={data.viewItemURL}> Buy</a></h4>
    </div>

    <div className="card-footer text-muted" style={{textAlign:"right", display: this.state.display2, backgroundColor: "rgb(229, 227,227)"}}>
    <IconButton className="mr-2"style={{ color:"rgb(35, 54, 86)", padding:"0"}}>
    <RemoveCircle onClick={()=> this.lessInfo()} />
    </IconButton>
    </div>

   </div>

  )
}
}

export default LikeButton
