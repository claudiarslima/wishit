// ----------- IMPORTS --------------------//
import React, { Component } from 'react';
import Center from 'react-center';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Button } from 'reactstrap';
import { LogIn } from '../../store/actions/authActions';
import  logo from '../../img/logo.jpg';
// --------------------------------------//


// ----------- CLASSE --------------------//
class Login extends Component {
  constructor(props) {
    super(props);
    //referência criada para animação do scroll-trigger aquando do click no botão "ENTRAR"
    this.myRef = React.createRef()
  }

  // STATE INICIAL
  state = {
    email: ' ',
    password: ' ',
  };

  //evento para ler o valor escrito nos inputs: email e password e atualizar, assim, o valor do state
  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value
    })
  }

  //evento para submeter o valor escrito nos inputs: email e password
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.LogIn(this.state);
    console.log(this.state);
  }
  render() {
    //usamos a const para fazer a verificação da autenticação na web-app e para processar eventuais erros no login
    const { auth, authError } = this.props;

    //quando o utilizador está logado é redirecionado para a página inicial
     if (auth.uid) {
       return <Redirect to='/PaginaInicial'/>;
     }

    return(
      <div>

      <div className="masthead text-white d-flex" style={{height: "100vh", backgroundColor:"rgb(123,155,166)", opacity:"59%"}}>
      <div className="container my-auto">
        <div className="row">
          <div className="col-lg-8 mx-auto text-center">
            <img src={logo} alt="Logotipo" className="img-fluid mt-4" />
              <Button onClick={()=>this._scrollDown()} className="btn mt-5 mx-auto" style={{ color: "white", backgroundColor: "#233656", borderRadius: "15px", borderColor: "#BD810C",}}>
            ENTRAR </Button>
          </div>
        </div>
      </div>
      </div>

    <section ref={this.myRef} style={{backgroundColor: "#CDD6D5"}}>
    <div className="container my-auto" style={{backgroundColor: "#CDD6D5", display: "flex", justifyContent: "center", alignItems: "center",  height: "100vh"}}>
        <div className="row">
            <div className="col-lg-12 mx-auto text-center">
                <form onSubmit={this.handleSubmit}>
                    <div className="form-group">
                    <label htmlFor="nome" style={{fontFamily: "Arial"}}>Email</label>
                    <input style={{backgroundColor: "#E5E3E3", borderRadius: "15px", width: "300px", fontFamily: "Arial"}}
                           type="email" className="form-control mx-auto" id="email" onChange={this.handleChange}
                           aria-describedby="emailHelp" placeholder="Insira o seu Email" />
                    <small id="emailHelp" className="form-text text-muted"></small>
                </div>

                <br/>


                  <div className="form-group">
                                        <label htmlFor="password" style={{fontFamily: "Arial"}}>Password</label>
                                        <input style={{backgroundColor: "#E5E3E3", borderRadius: "15px", width: "300px", fontFamily: "Arial"}}
                                               type="password" className="form-control mx-auto" onChange={this.handleChange} id="password"
                                               placeholder="Password" />
                                    </div>
                                    <br/>
                                    <small style={{fontFamily: "Arial"}}>Ainda não tem conta? Registe-se <Link style={{color: "#0c5460"}} to="/registo">aqui.</Link></small>
                                     <br/>
                                     <br/>
                  <input type="submit" value="VAMOS!" className="btn mx-auto" style={{backgroundColor: "#233656", borderRadius: "8px", borderColor: "#233656", color:"white", margin:"5px", maxWidth:"100px"}} />
                  <div className="red-text center"> {authError ? <p> { authError } </p> : null} </div>
                </form>


            </div>

        </div>

    </div>

</section>

</div>
    )
  }

//animação do scroll-trigger aquando do click no botão "ENTRAR"
  _scrollDown = () => {
    window.scrollTo({
            top: this.myRef.current.offsetTop-20,
            behavior: "smooth"
  })
}
}


//faz ligação aos dados da firebase
//dispara os erros do reducer
const mapStateToProps = (state) => {
    console.log(state);
  return {
    authError: state.auth.authError,
    auth: state.firebase.auth
  }
}


//faz a verificação do login
const mapDispatchToProps = (dispatch) => {
  return {
    LogIn: (creds) => dispatch(LogIn(creds))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
