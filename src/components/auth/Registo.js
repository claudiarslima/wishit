// ----------- IMPORTS --------------------//
import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { Registar } from '../../store/actions/authActions';
import { faUserCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// --------------------------------------//

// ----------- CLASSE --------------------//
class Registo extends Component {
      // STATE INICIAL
      state = {
      nome: ' ',
      username: ' ',
      email: ' ',
      password: ' '
  };

  //evento para ler o valor escrito nos inputs: email, password, nome e username e atualizar, assim, o valor do state
  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value
    })
  }

  //evento para submeter o valor escrito nos inputs: email, password, nome e username
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.Registar(this.state);
    console.log(this.state);
  }
  render() {

     //usamos a const para fazer a verificação da autenticação na web-app e para processar eventuais erros no login
     const { auth, authError } = this.props;

     //quando o utilizador está logado é redirecionado para a página inicial
     if (auth.uid) {
       return <Redirect to="/PaginaInicial" />
     }

  return(
    <div className="container" >

      <FontAwesomeIcon icon={faUserCircle} size="5x" style={{color: "#6B6B6B"}} className="mt-5 mb-3"/>

      <form onSubmit={this.handleSubmit}>

        <div className="form-group">
          <label htmlFor="nome" className="text-black-50"> Nome Completo </label>
          <input type="text" className="form-control mx-auto" id="nome" placeholder="Insira o seu Nome" onChange={this.handleChange}  style={{backgroundColor: "#E5E3E3", borderRadius: "15px", maxWidth: "300px", fontFamily: "Arial", fontSize: "13px"}} />
        </div>

        <div className="form-group">
          <label htmlFor="username" className="text-black-50"> Username </label>
          <input type="text" className="form-control mx-auto" id="username" placeholder="Insira um Username" onChange={this.handleChange} maxLength="20" style={{backgroundColor: "#E5E3E3", borderRadius: "15px", maxWidth: "300px", fontFamily: "Arial", fontSize: "13px"}} />
        </div>

         <div className="form-group">
           <label htmlFor="email" className="text-black-50"> Email  </label>
           <input type="email" className="form-control mx-auto" id="email" placeholder="Insira o seu Email" onChange={this.handleChange}  style={{backgroundColor: "#E5E3E3", borderRadius: "15px", maxWidth: "300px", fontFamily: "Arial", fontSize: "13px"}} />
         </div>

          <div className="form-group">
            <label htmlFor="password" className="text-black-50"> Password </label>
            <input type="password" className="form-control mx-auto" id="password" placeholder="Insira uma Password" onChange={this.handleChange}  style={{backgroundColor: "#E5E3E3", borderRadius: "15px", maxWidth: "300px", fontFamily: "Arial", fontSize: "13px"}} />
          </div>

           <input type="submit" value="REGISTAR" className="btn mx-auto" style={{backgroundColor: "#233656", borderRadius: "8px", borderColor: "#233656", color:"white", margin:"5px", maxWidth:"200px"}} />

           <div className="form-group red-text center">
           {authError ? <p> { authError } </p> : null}
           </div>

      </form>

      </div>
    )
  }
  }

  //faz ligação aos dados da firebase
  //dispara erros do reducer
  const mapStateToProps = (state) => {
    return {
      auth: state.firebase.auth,
      authError: state.auth.authError
    }
  }

  //faz a verificação do registo
  const mapDispatchToProps = (dispatch) => {
    return {
      Registar: (newUser) => dispatch(Registar(newUser))
    }
  }


export default connect(mapStateToProps, mapDispatchToProps)(Registo);
