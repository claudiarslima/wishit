// ----------- IMPORTS --------------------//
import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { LogOut } from '../../store/actions/authActions';
import { Button } from 'reactstrap';
import Navbar from '../../components/layout/Navbar';
import { faUserCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// --------------------------------------//


// ----------- CLASSE --------------------//
class User extends Component {

  // STATE INICIAL
  state = {
    estado: ' ',
    loading: true,
    authenticated: false,
    user: null,
    editar: false
  }

  //evento para fazer o click do botão de logout
  logout = (e) => {
    e.preventDefault();
    this.props.LogOut(this.state);
  }

  //evento para fazer o click do botão de editar dados
  editar = (e) => {
    this.setState({ editar: true });
  }

  render() {


    const { voltar, loading, authenticated, editar } = this.state;
    const { auth, profile } = this.props;

    //quando o utilizador não está logado é redirecionado para a página de login
    if (!auth.uid) {
      return <Redirect to='/login'/>;
    }

    //quando o utilizador clica no botão "editar dados" é redirecionado para a página onde pode fazê-lo
     if (editar) {
       return <Redirect to='/EditarDados'/>;
     }

  return(
    <div className="container">
      <Navbar />
      <p />
        <label style={{fontFamily: "Arial, bold", fontSize: "30px", color: "rgb(35, 54, 86)"}} className="mt-5">Página de Utilizador</label>
        <hr />
      <FontAwesomeIcon icon={faUserCircle} size="5x" style={{color: "rgb(35, 54, 86)"}} className="mt-4 mb-3"/>
      <form readOnly>
        <div className="form-group">
        <label htmlFor="nome" className=""  style={{color: "rgb(35, 54, 86)"}}> Nome Completo </label>
          <input type="text" readOnly className="form-control mx-auto" id="nome" placeholder={profile.nome} style={{backgroundColor: "white", borderRadius: "15px", maxWidth: "300px", fontFamily: "Arial", fontSize: "13px"}} />
        </div>

          <div className="form-group">
          <label htmlFor="username" className=""  style={{color: "rgb(35, 54, 86)"}}> Username </label>
            <input type="text" readOnly className="form-control mx-auto" id="username" placeholder={profile.username} maxLength="20" style={{backgroundColor: "white", borderRadius: "15px", maxWidth: "300px", fontFamily: "Arial", fontSize: "13px"}} />
          </div>

            <div className="form-group">
            <label htmlFor="email" className=""  style={{color: "rgb(35, 54, 86)"}}> Email  </label>
              <input type="email" readOnly className="form-control mx-auto" id="email" placeholder={auth.email} style={{backgroundColor: "white", borderRadius: "15px", maxWidth: "300px", fontFamily: "Arial", fontSize: "13px"}} />
            </div>

            <br/>
            <br />
            <Button onClick={this.editar} className="btn mx-auto" style={{backgroundColor: "#233656", borderRadius: "8px", borderColor: "#233656", color:"white", margin:"5px", maxWidth:"200px"}}>
          EDITAR DADOS </Button>
          <br />

           <input onClick={this.logout} value="TERMINAR SESSÃO" className="btn mx-auto" style={{backgroundColor: "#233656", borderRadius: "8px", borderColor: "#233656", color:"white", margin:"5px", maxWidth:"200px"}} />
           <div className="form-group red-text center">
           </div>
      </form>
      </div>
    )
  }
  }


  const mapStateToProps = (state) => {
    return {
         auth: state.firebase.auth,
         profile: state.firebase.profile
    }
  }

//faz verificaço do logout
const mapDispatchToProps = (dispatch) => {
    return {
        LogOut: () => dispatch(LogOut())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(User);
