// ----------- IMPORTS --------------------//
import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { firebase } from '../../config/fbConfig';
import { connect } from 'react-redux';
import { Button } from 'reactstrap';
import { Alert } from 'reactstrap';
import Navbar from '../../components/layout/Navbar';
import { Editar } from '../../store/actions/authActions';
import { faUserCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// --------------------------------------//


// ----------- CLASSE --------------------//
class EditarDados extends Component {
  constructor(props) {
    super(props)
  // STATE INICIAL
  this.state = {
    nome: ' ',
    username: ' ',
    alterar: false,
    visible: false
  }

}

  //evento para ler o valor escrito nos inputs: email, password, nome e username e atualizar, assim, o valor do state
  handleChange = (e) => {

    this.setState({
        nome: e.target.value
    })

      console.log(this.state);
  }

  handleChange1 = (e) => {

    this.setState({
      username: e.target.value
    })

      console.log(this.state);
  }


  //evento para submeter o valor escrito nos inputs: nome e username
  handleSubmit = (e) => {
    e.preventDefault();
    if (this.state.nome === ' ' || this.state.username === ' ') {
      this.setState({ visible: true });
  } else {
    this.props.Editar(this.state);
    console.log(this.state);
    this.setState({alterar: true})
  }
  }

  onDismiss = () => {
    this.setState({ visible: false });
  }

  render() {

    //usamos a const para fazer a verificação da autenticação na web-app e para processar eventuais erros no login
    const { auth, profile } = this.props;


    //quando o utilizador não está logado é redirecionado para a página de login
    if (!auth.uid) {
      return <Redirect to='/login'/>;
    }

    if(this.state.alterar == true) {
        return <Redirect to='/PaginaInicial'/>;
    }

  return(
    <div className="container">
    <Alert color="warning" isOpen={this.state.visible} toggle={this.onDismiss}>
        Preencha ambos os campos!
      </Alert>
      <Navbar />
      <br />
      <label style={{fontFamily: "Arial, bold", fontSize: "30px", color: "rgb(35, 54, 86)"}} className="mt-5">Edição de Dados</label>
      <hr />
      <small style={{fontFamily: "Arial", fontSize: "15px", color: "rgb(35, 54, 86)"}}> Aqui pode editar os dados que forneceu aquando do seu registo. </small>
      <hr />
      <FontAwesomeIcon icon={faUserCircle} size="5x" style={{color: "rgb(35, 54, 86)"}} className="mt-4 mb-3"/>
      <form onSubmit={this.handleSubmit}>
      <br/>
        <div className="form-group">
        <label htmlFor="nome" className=""  style={{color: "rgb(35, 54, 86)"}}> Nome Completo </label>
          <input type="text" className="form-control mx-auto" id="nome" placeholder="Alterar Nome" onChange={this.handleChange} style={{backgroundColor: "white", borderRadius: "15px", maxWidth: "300px", fontFamily: "Arial", fontSize: "13px"}} />
        </div>

          <div className="form-group">
          <label htmlFor="username" className=""  style={{color: "rgb(35, 54, 86)"}}> Username </label>
            <input type="text" className="form-control mx-auto" id="username" placeholder="Alterar Username" onChange={this.handleChange1} maxLength="20" style={{backgroundColor: "white", borderRadius: "15px", maxWidth: "300px", fontFamily: "Arial", fontSize: "13px"}} />
          </div>
          <br />
            <input type="submit" value="EDITAR DADOS" className="btn mx-auto" style={{backgroundColor: "#233656", borderRadius: "8px", borderColor: "#233656", color:"white", margin:"5px", maxWidth:"200px"}} />
      </form>
      </div>
    )
  }
}

  //faz ligação aos dados da firebase
  //faz verificação da edição de dados
  const mapStateToProps = (state) => {
    console.log(state);
    return {
         auth: state.firebase.auth,
         profile: state.firebase.profile
    }
  }

  //faz a verificação da edição de dados
  const mapDispatchToProps = (dispatch) => {
    return {
      Editar: (data) => dispatch(Editar(data))
    }
  }

export default connect(mapStateToProps, mapDispatchToProps)(EditarDados);
