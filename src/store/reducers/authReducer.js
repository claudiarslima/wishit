
//state inicial
const initState = {
  authError: null
}

//processamento de erros relacionados com a autenticação
const authReducer = (state = initState, action) => {
  switch(action.type) {
    case 'USER_REGISTADO':
    console.log('user registado');
    return {
      ...state,
      authError: null
    }
    case 'ERRO_REGISTO':
    console.log('erro no registo');
    return {
      ...state,
      authError: action.err.message
    }
    case 'LOGIN_EFETUADO':
    console.log('login efetuado');
    return {
      ...state,
      authError: null,

    }
    case 'ERRO_LOGIN':
    console.log('erro no login');
    return {
      ...state,
      authError: 'Erro no Login'
    }
    case 'SIGNOUT_SUCCESS':
    console.log('signout success');
    return state;
    case 'DADOS_ALTERADOS':
    console.log('dados alterados');
    return {
      ...state,
      authError: null
    }
    case 'ERRO_ALTERAR':
    console.log('erro ao alterar os dados');
    return state;
    default:
    return state;
  }
}

export default authReducer;
