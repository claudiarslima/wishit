// ----------- IMPORTS --------------------//
import authReducer from './authReducer'
import likeReducer from './likeReducer'
import viewReducer from './viewReducer'
import { combineReducers } from 'redux'
import { firestoreReducer } from 'redux-firestore'
import { firebaseReducer } from 'react-redux-firebase'
// --------------------------------------//


const rootReducer = combineReducers({
  auth: authReducer,
  like: likeReducer,
  view: viewReducer,
  firestore: firestoreReducer,
  firebase: firebaseReducer
})

export default rootReducer;
