const initState = {
  views: [{
    idview: '1',
    imgview: 'img/img.png',
    nameview: 'teste'
  }]
}

const viewReducer = (state = initState, action) => {
  switch (action.type) {
    case 'ADD_VIEW':
      console.log("view adicionada", action.view);
      return state;

    case 'ERRO_VIEW':
    console.log("erro na view", action.err);
    return state;

    default:
    return state;
  }
}

export default viewReducer;
