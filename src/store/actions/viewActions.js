
export const Views = (view) => {
  return(dispatch, getState, {getFirebase, getFirestore}) => {

    const firestore = getFirestore();
    const profile = getState().firebase.profile;
    const userId = getState().firebase.auth.uid;

    firestore.collection('views').add({
      idview: view.idview,
      imgview: view.imgview,
      nomeview: view.nomeview,
      userId: userId,
      createdAt: new Date()
    }).then(() => {
      console.log('adicionou' + view)
      dispatch({type: 'ADD_VIEW', id:view})
    }).catch(err => {
      console.log('erro na view')
      dispatch({type: 'ERRO_VIEW'})
    })
  }
}
