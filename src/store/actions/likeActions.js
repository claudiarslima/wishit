
export const Likes = (like) => {
  return(dispatch, getState, {getFirebase, getFirestore}) => {

    const firestore = getFirestore();
    const profile = getState().firebase.profile;
    const userId = getState().firebase.auth.uid;

    firestore.collection('likes').add({
      idprod: like.idprod,
      imgprod: like.imgprod,
      nomeprod: like.nomeprod,
      priceprod: like.priceprod,
      userId: userId,
      createdAt: new Date(),
      like: true
  }).then(() => {
    dispatch({type: 'ADD_LIKE', id:like})
  }).catch(err=>{
    console.log('error')
    dispatch({type: 'ERRO_LIKE'})
  })
  }
}

export const Delete = (like) => {
  return(dispatch, getState, {getFirebase, getFirestore}) => {
    const firebase = getFirebase();
    const firestore = getFirestore();

    firestore.collection('likes').where("idprod", "==", like.idprod).where("userId", "==", like.userId).get()
    .then(snapshot => {
      snapshot.forEach(doc => {
        firestore.collection('likes').doc(doc.id).delete();
      })
      dispatch({type: 'DELETE'})
    }).catch(err => {
      console.log('erro no delete')
      dispatch({type: 'ERRO_DELETE'})
    })
  }
}
