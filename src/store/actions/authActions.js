
// -------------------------- PÁGINA QUE PROCESSA VERIFICAÇÕES A NÍVEL DO LOGIN, LOGOUT, REGISTO E EDIÇÃO DE DADOS -------------------------------------------------//

//faz a verificação do login
export const LogIn = (credentials) => {
  return(dispatch, getState, {getFirebase}) => {
    const firebase = getFirebase();

      firebase.auth().signInWithEmailAndPassword(
        credentials.email,
        credentials.password
      ).then(() => {
        dispatch({ type: 'LOGIN_EFETUADO'})
      }).catch((err) => {
        dispatch({ type: 'ERRO_LOGIN', err})
      });
  }

}

//faz a verificação do logout
export const LogOut = () => {
    return (dispatch, getState, {getFirebase}) => {
        const firebase = getFirebase();

        firebase.auth().signOut().then(() => {
            dispatch({type: 'SIGNOUT_SUCCESS'});

        });
    }
}

//faz a verificação do resgisto
export const Registar = (newUser) => {
  return(dispatch, getState, {getFirebase, getFirestore}) => {
    const firebase = getFirebase();
    const firestore = getFirestore();

    firebase.auth().createUserWithEmailAndPassword(
      newUser.email,
      newUser.password
    ).then((resp) => {
      return firestore.collection('users').doc(resp.user.uid).set({
        nome: newUser.nome,
        username: newUser.username
      })
    }).then(() => {
      dispatch({ type: 'USER_REGISTADO'})
    }).catch(err => {
      dispatch({ type: 'ERRO_REGISTO', err})
    } )
  }
}

//faz a verificação da edição de dados
export const Editar = (data) => {
  return(dispatch, getState, {getFirebase, getFirestore}) => {
    const firebase = getFirebase();
    const firestore = getFirestore();
    const user = firebase.auth().currentUser;

    if (user) {
    firestore.collection('users').doc(user.uid).update({
        nome: data.nome,
        username: data.username
    }).then(() => {
        dispatch({ type : 'DADOS_ALTERADOS'})
      }).catch(err => {
        dispatch({ type: 'ERRO_ALTERAR'})
      })
  } else {
    console.log("erro ao alterar");
  }
}
}
